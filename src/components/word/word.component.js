import './word.styl';
import template from './word.html';

let word = {
  bindings: {
    letters: '<'
  },
  template: template,
  controllerAs: 'word'
};

export default word;
