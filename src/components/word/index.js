import angular from 'angular';

import word from './word.component';

export default angular.module('word', [])
  .component('word', word)
  .name;
