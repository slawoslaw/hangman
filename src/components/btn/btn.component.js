import './btn.styl';
import template from './btn.html';

let btn = {
  transclude: true,
  template: template
};

export default btn;
