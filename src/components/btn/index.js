import angular from 'angular';

import btn from './btn.component';

export default angular.module('btn', [])
  .component('btn', btn)
  .name;
