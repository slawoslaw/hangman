import './board.styl';
import template from './board.html';

const board = {
  bindings: {
    availableLetters: '<',
    checkedLetters: '<',
    onCheckLetter: '&'
  },
  template: template,
  controller: 'BoardCtrl',
  controllerAs: 'board'
};

export default board;
