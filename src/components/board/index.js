import angular from 'angular';

import board from './board.component';
import BoardCtrl from './board.controller';

export default angular.module('board', [])
  .component('board', board)
  .controller('BoardCtrl', BoardCtrl)
  .name;
