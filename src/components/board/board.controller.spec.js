import board from './';

describe('board', () => {

  describe('BoardCtrl', () => {
    let ctrl;

    beforeEach(() => {
      angular.mock.module(board);

      angular.mock.inject(($controller) => {
        ctrl = $controller('BoardCtrl', {});
      });

      ctrl.availableLetters = 'abcdefghijklmnopqrstuvwxyz';
    });

    it('should start with empty letters array', () => {
      expect(ctrl.letters.length).toBe(0);
    });

    it('should generate letters array', () => {
      ctrl.generateLettersArray();

      expect(ctrl.letters.length).toBe(ctrl.availableLetters.length);
      expect(ctrl.letters[0].character).toBe(ctrl.availableLetters[0]);
    });

    it('should call generateLettersArray function on init ', () => {
      spyOn(ctrl, 'generateLettersArray')

      ctrl.$onInit();

      expect(ctrl.generateLettersArray).toHaveBeenCalled();
    });
  });
});
