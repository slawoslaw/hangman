class BoardCtrl {
  constructor() {
    this.letters = [];
  }

  $onInit() {
    this.generateLettersArray();
  }

  generateLettersArray() {
    if (this.availableLetters) {
      angular.forEach(this.availableLetters, (letter) => {
        this.letters.push({
          character: letter,
          selected: false
        });
      });
    }
  }

  checkLetter(letter) {
    if (!letter.selected) {
      this.onCheckLetter({character: letter.character});
      letter.selected = true;
    }
  }
}

export default BoardCtrl;
