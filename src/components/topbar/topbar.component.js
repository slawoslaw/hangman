import './topbar.styl';
import template from './topbar.html';

const topBar = {
  bindings: {
    title: '@',
    level: '<',
    attempts: '<',
    fails: '<',
    maxFails: '<',
    lettersLeft: '<'
  },
  template: template,
  controllerAs: 'topBar'
};

export default topBar;
