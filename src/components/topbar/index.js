import angular from 'angular';

import topBar from './topbar.component';

export default angular.module('topbar', [])
  .component('topBar', topBar)
  .name;
