import './modal.styl';
import template from './modal.html';

const modal = {
  require: {
    parent: '^app'
  },
  template: template,
  controller: 'ModalCtrl',
  controllerAs: 'modal'
};

export default modal;
