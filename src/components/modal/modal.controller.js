class ModalCtrl {
  constructor($scope, ModalService) {
    this.modalService = ModalService;
    this.reset();

    $scope.$on('launch modal', (event, activeModal, name, params) => {
      this.launched = true;
      this.type = name;
      this.title = activeModal.title;
      this.msg = activeModal.msg;
      this.params = params;
    });
  }

  reset() {
    this.launched = false;
    this.type = '';
    this.title = '';
    this.msg = '';
    this.params = {};
  }

  close() {
    this.reset();
    this.modalService.close();
    this.parent.game.init();
  }

  newGame() {
    this.reset();
    this.modalService.close();
    this.parent.game.start();
  }

  nextLevel() {
    this.reset();
    this.modalService.close();
    this.parent.game.nextLevel();
  }
}

ModalCtrl.$inject = ['$scope', 'ModalService'];

export default ModalCtrl;
