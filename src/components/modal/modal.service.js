class ModalService {
  constructor($rootScope) {
    this.$rootScope = $rootScope;
    this.modals = {
      fail: {
        title: 'You failed!',
        msg: 'Start new game and try again!'
      },
      level: {
        title: 'Success!',
        msg: 'You passed the next level!'
      },
      success: {
        title: 'Success!',
        msg: 'You passed all levels!'
      }
    }
    this.activeModal = null;
  }

  launch(name, params) {
    let modal = this.modals[name];
    this.activeModal = modal;
    this.$rootScope.$broadcast('launch modal', this.activeModal, name, params);
  }

  close() {
    this.activeModal = null;
  }
}

ModalService.$inject = ['$rootScope'];

export default ModalService;
