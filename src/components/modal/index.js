import angular from 'angular';

import modal from './modal.component';
import ModalCtrl from './modal.controller';
import ModalService from './modal.service';

export default angular.module('modal', [])
  .component('modal', modal)
  .controller('ModalCtrl', ModalCtrl)
  .service('ModalService', ModalService)
  .name;
