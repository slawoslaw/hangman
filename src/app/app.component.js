import './app.styl';
import template from './app.html';

let app = {
  template: template,
  controller: 'AppCtrl',
  controllerAs: 'app'
};

export default app;
