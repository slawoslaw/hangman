import angular from 'angular';

import game from '../common/game';
import btn from '../components/btn';
import topbar from '../components/topbar';
import word from '../components/word';
import board from '../components/board';
import modal from '../components/modal';

import app from './app.component';
import AppCtrl from './app.controller';

export default angular.module('app', [game, btn, topbar, word, board, modal])
  .component('app', app)
  .controller('AppCtrl', AppCtrl)
  .name;
