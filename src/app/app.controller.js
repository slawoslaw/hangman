class AppCtrl {
  constructor(GameService) {
    this.title = 'Hangman';
    this.game = GameService;
  }
}

AppCtrl.$inject = ['GameService'];

export default AppCtrl;
