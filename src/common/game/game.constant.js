const GameConstant = {
  STATUS: {
    INIT: 'init',
    GAME: 'game',
    SUCCESS: 'success',
    FAIL: 'fail'
  }
};

export default GameConstant;
