class GameService {
  constructor($http, GameConstant, ModalService) {
    this.http = $http;
    this.modalService = ModalService;
    this.STATUS = GameConstant.STATUS;
    this.availableLetters = 'abcdefghijklmnopqrstuvwxyz';
    this.init();
  }

  init() {
    this.status = this.STATUS.INIT;
    this.fails = 0;
    this.maxFails = 6;
    this.attempts = 0;
    this.lettersLeft = 0;
    this.startTime = 0;
    this.endTime = 0;
    this.level = null;
    this.lastLevel = 6;
    this.word = '';
    this.letters = [];
    this.checkedLetters = [];
  }

  started() {
    return this.status === this.STATUS.GAME;
  }

  generateWord() {
    this.http.get('/words.json').then((response) => {
      let words = response.data;
      this.word = words[Math.floor(Math.random() * words.length)];
      this.lettersLeft = angular.copy(this.word.length);

      angular.forEach(this.word, (letter) => {
        this.letters.push({
          character: letter,
          solved: false
        });
      });

      console.info('The word is:', this.word);
    });
  }

  start(level = 1) {
    this.status = this.STATUS.GAME;
    this.fails = 0;
    this.attempts = 0;
    this.lettersLeft = 0;
    this.startTime = new Date();
    this.level = level;
    this.letters = [];
    this.checkedLetters = [];
    this.generateWord();
  }

  nextLevel() {
    this.start(this.level + 1);
  }

  gameDuration() {
    this.endTime = new Date();
    return Math.round((this.endTime - this.startTime) / 1000);
  }

  checkLetter(character) {
    if (this.status == this.STATUS.GAME && this.checkedLetters.indexOf(character) === -1) {
      let params = {
        duration: null
      };

      this.checkedLetters.push(character);
      this.attempts++;

      if (this.word.indexOf(character) === -1) {
        this.fails++;

        if (this.fails === this.maxFails) {
          this.status = this.STATUS.FAIL;
          params.duration = this.gameDuration();
          this.modalService.launch('fail', params);
        }

        return false;
      }
      else {
        this.lettersLeft = angular.copy(this.word.length);

        angular.forEach(this.letters, (letter) => {
          if (!letter.solved) {
            if (letter.character === character) {
              letter.solved = true;
              this.lettersLeft--;
            }
          }
          else {
            this.lettersLeft--;
          }
        });

        if (this.lettersLeft === 0) {
          this.status = this.STATUS.SUCCESS;
          params.duration = this.gameDuration();

          if (this.level === this.lastLevel) {
            this.modalService.launch('success', params);
          }
          else {
            this.modalService.launch('level', params);
          }
        }

        return true;
      }
    }
    else {
      return false;
    }
  }
}

GameService.$inject = ['$http', 'GameConstant', 'ModalService'];

export default GameService;
