import angular from 'angular';

import modal from '../../components/modal';

import GameConstant from './game.constant';
import GameService from './game.service';

export default angular.module('game', [modal])
  .constant('GameConstant', GameConstant)
  .service('GameService', GameService)
  .name;
