import game from './';

describe('game', () => {

  describe('GameService', () => {
    let STATUS, gameService, httpBackend;

    beforeEach(() => {
      angular.mock.module(game);

      angular.mock.inject((GameConstant, GameService, $httpBackend) => {
        STATUS = GameConstant.STATUS;
        gameService = GameService;
        httpBackend = $httpBackend;
      });
    });

    it('should start with no fails', () => {
      expect(gameService.fails).toEqual(0);
    });

    it('should start with zero attempts', () => {
      expect(gameService.attempts).toEqual(0);
    });

    it('should check letter only when game started', () => {
      let checkedLetter = gameService.checkLetter('a');
      expect(checkedLetter).toBeFalsy();
    });

    describe('game started', () => {
      beforeEach(function() {
        httpBackend.expect('GET', '/words.json');
        httpBackend.whenGET('/words.json').respond(200, ["temporary"]);
      });

      afterEach(function() {
        httpBackend.verifyNoOutstandingExpectation();
        httpBackend.verifyNoOutstandingRequest();
      });

      it('should start on first level', () => {
        gameService.start();

        httpBackend.flush();
        expect(gameService.level).toEqual(1);
      });

      it('should start timer', () => {
        gameService.start();
        let now = new Date;

        httpBackend.flush();
        expect(gameService.status).toBe(STATUS.GAME);
        expect(gameService.startTime.toTimeString()).toBe(now.toTimeString());
      });

      it('should generate word', () => {
        gameService.generateWord();

        httpBackend.flush();
        expect(gameService.word).toBe('temporary');
        expect(gameService.letters.length).toBe(gameService.word.length);
        expect(gameService.letters[0].character).toBe(gameService.word[0]);
        expect(gameService.letters[0].solved).toBeFalsy();
      });

      it('should check letter that was not checked earlier', () => {
        gameService.start();

        httpBackend.flush();

        spyOn(gameService, 'checkLetter').and.callThrough();
        gameService.checkLetter('a');
        let checkedLetter = gameService.checkLetter('a');
        expect(checkedLetter).toBeFalsy();
      });

      it('should store checked letter', () => {
        gameService.start();
        gameService.checkLetter('a');

        httpBackend.flush();
        expect(gameService.checkedLetters).toContain('a');
      });

      it('should update letter list', () => {
        gameService.start();

        httpBackend.flush();
        let checkedLetter = gameService.checkLetter('r');
        expect(gameService.letters[5].solved).toBeTruthy();
        expect(gameService.letters[7].solved).toBeTruthy();
        expect(checkedLetter).toBeTruthy();
      });

      it('should increase fail list', () => {
        gameService.start();

        httpBackend.flush();
        let checkedLetter = gameService.checkLetter('z');
        expect(gameService.fails).toEqual(1);
        expect(checkedLetter).toBeFalsy();
      });
    });
  });
});
